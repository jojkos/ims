# Project: IMS
# Authors: Marek Sychra (xsychr05), Jonáš Holcner (xholcn01); FIT
CPP=g++
FLAGS=-std=c++11 -Wall -pedantic -g -O2
LIBS=-lsimlib -lm

all: vcely

vcely: vcely.cc vcely.h
	$(CPP) vcely.cc -o vcely $(FLAGS) $(LIBS)
run: vcely
	./vcely

clean:
	rm vcely vcely.dat


