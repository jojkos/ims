#include "simlib.h"
#include "vcely.h"

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <math.h>
using namespace std;


//nastavovaci promenne  //vychozi hodnoty
#define PRACOVKYNE  100 //100
#define LETAVKY 50      //50
#define DELKABEHU ROK
#define KAPACITAPLASTVI 1280 //kolik se tam vejde med+vejce+larvy ->>>>1280, jedna bunka je na velikost bunky larev, ma asi 250mm na druhou
#define MAXVCEL 600 //600

int pocetKrmicek = 0;
double med = 150; //150
double pocatecniMed = med;
double nabranyMed = 0;
int pocetZarodku = 0; //vajce+larvy

double jakCastoJist = 0.1*DEN;
double jakDlouhoKrmi = 0.1*DEN;
double kdyVybiraMed = 0.90 ;
double sanceNaSpareni = 0.75;

bool vypisky = false;



//pomocne promenne
vector<Vcela *> zijiciVcely; //pro rojeni a nemoc
Queue frontaVickovani;
Kralovna * zijiciKralovna = NULL;
Varoaza * varoaza = NULL;


///STATISTIKY
TStat RozeniKralovny("rozeni kralovny");
Stat ObratMedu("obrat medu");
Histogram DelkaZivota("Delka zivota vcel", 0, 1, 365);

int pocetVybraniMedu = 0;
int pocetRojeni = 0;
int zabitychVaroazou = 0;
int pocetZarodkuHlad = 0;
int pocetZarodkuKrmicky = 0;
int pocetVcelyHlad = 0;
int pocetVcelyKrmicky = 0;
int celkoveVcely = 0;
vector<double>DatumVaroazy;

double DenniStavMedu[DELKABEHU/DEN];
int DenniStavVcel[DELKABEHU/DEN];
int DenniStavZarodku[DELKABEHU/DEN];
int DenniStavTrubcu[DELKABEHU/DEN];
int pocetTrubcu = 0; //VCETNE VAJEC


//Cancel() nerusi ocekavanym zpusobem, reseno pomoci eventu a passivate
///zpusobilo zustavani veci v pameti

//pro ziskavani statistik
class HlasicDne : public Event {
public:
	int counter = 0;
	HlasicDne():Event(3){
		Activate(Time+DEN);
	}
	void Behavior(){
		DenniStavMedu[counter] = med;
		DenniStavVcel[counter] = zijiciVcely.size();
		DenniStavZarodku[counter] = pocetZarodku;
		DenniStavTrubcu[counter] = pocetTrubcu;

		if (vypisky)
			cout<<"NOVY DEN "<<Time/DEN<<"\n";
		counter += 1;
		Activate(Time+DEN);
	}
};


int jakyMesic(long cas){
	int modCas = (long)Time%ROK;

	if (modCas >= 0 && modCas < 31*DEN)//leden
		return Leden;
	else if (modCas >= 31*DEN && modCas < 59*DEN)//unor
		return Unor;
	else if (modCas >= 59*DEN && modCas < 90*DEN)//brezen
		return Brezen;
	else if (modCas >= 90*DEN && modCas < 120*DEN)//duben
		return Duben;
	else if (modCas >= 120*DEN && modCas < 151*DEN)//kveten
		return Kveten;
	else if (modCas >= 151*DEN && modCas < 181*DEN)//cerven
		return Cerven;
	else if (modCas >= 181*DEN && modCas < 212*DEN)//cervenec
		return Cervenec;
	else if (modCas >= 212*DEN && modCas < 243*DEN)//srpen
		return Srpen;
	else if (modCas >= 243*DEN && modCas < 273*DEN)//zari
		return Zari;
	else if (modCas >= 273*DEN && modCas < 304*DEN)//rijen
		return Rijen;
	else if (modCas >= 304*DEN && modCas < 334*DEN)//listopad
		return Listopad;
	else if (modCas >= 334*DEN && modCas < 365*DEN)//prosinec
		return Prosinec;
	else{
		std::cout<<"NEJAKA ZASADNI CASOVA CHYBA\n";
		return 0;
	}
}


int produkceVajec(){ //jak dlouho kralovna ceka nez rodi znova
	//return DEN/5;
	switch (jakyMesic((long)Time)){
		case Leden:
			return DEN/Uniform(45,55);
		case Unor:
			return DEN/Uniform(90,110);
		case Brezen:
			return DEN/Uniform(315,385);
		case Duben:
			return DEN/Uniform(900,1100);
		case Kveten:
			return DEN/Uniform(1440,1760);
		case Cerven:
			return DEN/Uniform(1530,1870);
		case Cervenec:
			return DEN/Uniform(675,825);
		case Srpen:
			return DEN/Uniform(540,660);
		case Zari:
			return DEN/Uniform(90,110);
		case Rijen:
			return DEN/Uniform(18,22);
		case Listopad:
			return 0;
		case Prosinec:
			return 0;
		default:
			std::cout<<"chyba mesice vajce\n";
			return 0;
	}
}

double kolikMamJist(){
	switch (jakyMesic((long)Time)){
		case Leden:
			return 0.001;
		case Unor:
			return 0.001;
		case Brezen:
			return 0.002;
		case Duben:
			return 0.002;
		case Kveten:
			return 0.002;
		case Cerven:
			return 0.002;
		case Cervenec:
			return 0.002;
		case Srpen:
			return 0.002;
		case Zari:
			return 0.002;
		case Rijen:
			return 0.002;
		case Listopad:
			return 0.0015;
		case Prosinec:
			return 0.0015;
		default:
			std::cout<<"chyba mesice kolik jist\n";
			return 0;
	}
}

double produkceJidla(){
	switch (jakyMesic((long)Time)){
		case Leden:
			return 0.; //kde je nula, stejne nelitaji
		case Unor:
			return 0.;
		case Brezen:
			return 0.03;
		case Duben:
			return 0.04;
		case Kveten:
			return 0.04;
		case Cerven:
			return 0.05;
		case Cervenec:
			return 0.04;
		case Srpen:
			return 0.04;
		case Zari:
			return 0.03;
		case Rijen:
			return 0.;
		case Listopad:
			return 0.;
		case Prosinec:
			return 0.;
		default:
			std::cout<<"chyba mesice produkce jidlo\n";
			return 0;
	}
}

double bunkyNaKilaMedu(double bunkyMedu){
    return bunkyMedu * 100 * 255 / 1000 / 1000/ 1000 * 1417; //TODO nejak okomentovat
}


void pridatMed(double mnozstvi){ ///STATISTIKY
	if (vypisky)
		cout<<"donesly med "<<med<<"\n";
	med += mnozstvi;
	ObratMedu(med);
  
	if (((KAPACITAPLASTVI - pocetZarodku) * kdyVybiraMed) < med){
		//STATISTIKY, vcelar odnesl med
		pocetVybraniMedu += 1;
		if (vypisky)
			cout<<"Vcelar ODNASI MED: "<<med<<"\n";
		nabranyMed += med*0.9;
		med = med*0.1;
		if (vypisky)
			cout<<"Vcelar ODNESL MED: "<<med<<"\n";
	}
}

void odebratMed(double mnozstvi){
	med -= mnozstvi;
	ObratMedu(med);
}

void oddelatZeVcel(Vcela * vcelka){
	int index = 0;
	for (auto i : zijiciVcely){
		if (i == vcelka){
			zijiciVcely.erase((zijiciVcely.begin() + index));
		}
		index += 1;
	}
}




class Rojeni : public Process {
public:
	Rojeni():Process(2){ //vyssi priorita
	}

	void Behavior(){
		pocetRojeni += 1;
		if (vypisky)
			cout<<"rojeni\n";

		if (zijiciKralovna != NULL){
			zijiciKralovna->Cancel();
		}
		//vyroji pulka
		int pulka = zijiciVcely.size()/2;
		for (int i = 0; i < pulka; i++){
			//kazda vcela si odnese na cestu nakej med
			odebratMed(produkceJidla());
			int index = Uniform(0,zijiciVcely.size()-1);
			zijiciVcely[index]->Cancel(); //vyrojila->umrela
		}
	}
};

class Smrt : public Event {
	Vcela *vcelicka;
public:
	Smrt(Vcela *p, double dt):vcelicka(p){
		Activate(Time+dt);
	}
	void Behavior(){
		if (vypisky)
			cout<<"UMRELA NA STARI "<<vcelicka->Name()<<"\n";
		vcelicka->smrt = NULL;
		vcelicka->Cancel();
		Cancel();
	}
};

class Timeout : public Event {
public:
	Vcela *vcela;
	Timeout(Vcela *p) : vcela(p)
	{
	}
	void Behavior(){
		if (vypisky)
			cout<<"timeout "<<Time<< "\n";
		vcela->Activate();
	}
};

class Zabij : public Event {
public:
	Vcela *vcela;
	Zabij(Vcela *p) : vcela(p)
	{
	}
	void Behavior(){
		vcela->Cancel();
        Cancel();
	}
};

class OdstraneniVaroazy : public Event {
public:
	void Behavior(){
		if (vypisky)
			cout << "RUSIM VAROAZU JESTLI JE\n";
		if (varoaza != NULL){
			varoaza->Cancel();
			varoaza = NULL;
		}
		Activate(Time+ROK);
	}
};
       
Varoaza::Varoaza(){
	narozeni = Time;
	interval = 30*DEN;
}

void Varoaza::Behavior(){
  DatumVaroazy.push_back(Time);
	if (vypisky)
		cout<<"VAROAZA ZABIJI\n";
	if (zijiciVcely.size() == 0){
		Cancel();
	}
	else{
		if (zijiciVcely.size() == 1){
			zijiciVcely[0]->Cancel();
		}
		else{
			zijiciVcely[Uniform(0,zijiciVcely.size()-1)]->Cancel(); //zabije nahodne vcelu
		}
		zabitychVaroazou += 1;   
		if (interval <= 0.2*DEN)
			interval =  0.2*DEN;
		else
			interval = interval*0.85; //jak rychle se varoaza siri
		Activate(Time+interval);
	}



}

void Vcela::NechatSeNakrmit(){
	if (pocetKrmicek == 0){
		if (stav == S_vejce || stav == S_larva || stav == S_kukla)
    		  pocetZarodkuKrmicky += 1;
		else
			pocetVcelyKrmicky += 1;
          
  		if (vypisky)
  			cout<<"MALO KRMICEK "<<Name()<<"\n";
        (new Zabij(this))->Activate();
        Passivate();
  		//Cancel();
	}
	else if(med <= kolikMamJist()){
    	if (stav == S_vejce || stav == S_larva || stav == S_kukla)
    		  pocetZarodkuHlad += 1;
    	else
    		pocetVcelyHlad += 1;
    
		if (vypisky)
			cout<<"MALO MEDU nechatSeKrmit "<<Name()<<"\n";
		(new Zabij(this))->Activate();
		Passivate();
		//Cancel();
	}
	else{
		//if (vypisky)
			//cout<<"nechatSeKrmit "<<Name()<<" jidlo:"<<med<<" krmicky "<<pocetKrmicek<<"\n";
		pocetKrmicek -= 1;
		mamKrmicku = true;
		odebratMed(kolikMamJist());

		Wait(jakDlouhoKrmi);
		pocetKrmicek += 1;
		mamKrmicku = false;
	}
}

void Vcela::NajistSe(){
	if(med <= kolikMamJist()){
		pocetVcelyHlad += 1;
  
  		if (vypisky)
  			cout<<"MALO MEDU najistSe"<<Name()<<"\n";
  		(new Zabij(this))->Activate();
        Passivate();
  		//Cancel();
	}
	else{
		//if (vypisky)
			//cout<<"najistSe "<<Name()<<" jidlo:"<<med<<" krmicky "<<pocetKrmicek<<"\n";
		odebratMed(kolikMamJist());
		Wait(MINUTA);
	}
}

void Vcela:: NarozenaVcela(){
	zijiciVcely.push_back(this);
	if (vypisky)
		cout<<"narozenaVcela "<<Name()<<" pocetVcel: "<<zijiciVcely.size()<<"\n";
	if (zijiciVcely.size() >= MAXVCEL){
		(new Rojeni)->Activate();
	}
}

void Vcela::Behavior(){
  celkoveVcely += 1;
	narozeni = Time;

	///podle toho jako co se narodila, musim zapocitat do ulu
	if (stav == S_vejce || stav == S_larva || stav == S_kukla){
		pocetZarodku += 1;
		if (vypisky)
			cout<<"novy zarodek "<<Name()<<" pocet zarodku "<<pocetZarodku<<"\n";
	}
	else{
		NarozenaVcela();
	}
}

void Vcela::Cancel(){
	DelkaZivota((Time-narozeni)/DEN);

	if (mamKrmicku){ //vrati krmicku kdyz umrel behem krmeni
		pocetKrmicek += 1;
		cout<<"posmrtne vracena krmicka\n";
	}

	///podle toho jako co umrela, musim zapocitat do ulu
	if (stav == S_vejce || stav == S_larva || stav == S_kukla)
		pocetZarodku -= 1;
	else{
		if (zijiciVcely.size() < 0)
			cout<<"vcel je min jak nula\n";
		oddelatZeVcel(this);

	}
	if (vypisky)
		cout<< "UMIRA "<<Name()<<" pocet zarodku "<<pocetZarodku<<" pocet vcel "<<zijiciVcely.size()<<"\n";
	//PREDCASNY KONEC, vcely mrtve
	if (zijiciVcely.size() == 0)// && pocetZarodku == 0
		Stop();

	//samotna kralovna
	if (zijiciVcely.size() == 1 && zijiciKralovna != NULL){
		if (vypisky)
			cout<<"kralovna je posledni, nema ji kdo krmit\n";
		Stop();

	}
	Process::Cancel();
}


Trubec::Trubec():vek(Uniform(6*TYDEN,8*TYDEN)){
	stav = S_vejce;
	pocetTrubcu += 1;
}

Trubec::Trubec(Stav zacStav, int zacVek):vek(zacVek){
	stav = zacStav;
	pocetTrubcu += 1;
}

void Trubec::Behavior() {
	Vcela::Behavior();

	smrt = new Smrt(this,vek);

	while (true){
		switch (stav){
			case S_vejce:{
				Wait(Uniform(70*HODINA,76*HODINA)); //lihnuti vejce
				stav = S_larva;
				break;
			}
			case S_larva:{
				//zakukleni
				int counter = 0;
				while (counter < Uniform(6*DEN,8*DEN)){
					NechatSeNakrmit();
					Wait(jakCastoJist); //nez chce znova jist
					counter += jakCastoJist;
				}
				stav = S_kukla;
				break;
			}
			case S_kukla:{
				Wait(Uniform(12*DEN,14*DEN)); //lihnuti trubce
				stav = S_vylihle;
                        
				pocetZarodku -= 1;
				NarozenaVcela();        
				break;
			}
			case S_vylihle:{
				//dospivani
				int counter = 0;
				while (counter < Uniform(5*DEN,7*DEN)){
					NechatSeNakrmit();
					Wait(jakCastoJist);
					counter += jakCastoJist;
				}
				stav = S_dospele;
				break;
			}
			case S_dospele:{
				while (true){
					NajistSe();//naji se na cestu
					Wait(Exponential(0.5*DEN)); //leti a hleda

					if(Random() <= 0.66){ // pokud se nesparil
						if(Random() <= 0.1){ //donesl varoazu
							if (varoaza == NULL){
								varoaza = new Varoaza();
								varoaza->Activate();
							}
							else
								varoaza->Activate();
						}

					}
					else{ //sparil, umira
                        (new Zabij(this))->Activate();
                        Passivate();                           
						//Cancel();
					}
				}
				break;
			}
			default:
				if (vypisky)
					cout << "SPATNY STAV TRUBEC\n";
				break;
		}
	}

}

void Trubec::Cancel() {
	if (smrt != NULL)
		smrt->Cancel(); //aby se ji to nesnazilo zabijet po smrti
	pocetTrubcu -= 1;
	Vcela::Cancel();
}





Kralovna::Kralovna():vek(3*ROK){
	SetName("kralovna");
	stav = S_vylihle;
}

Kralovna::Kralovna(Stav zacStav, int zacVek):vek(zacVek){
	SetName("kralovna");
	stav = zacStav;
}

void Kralovna::Behavior(){
	Vcela::Behavior();
	zijiciKralovna = this;
	smrt = new Smrt(this,vek);

	while (true){
		switch (stav){
			case S_vylihle:{
				//dorusta
				int counter = 0;
				while (counter < Uniform(3*DEN,5*DEN)){
					NajistSe();//zjednoduseni pro modelovani
					//NechatSeNakrmit();
					Wait(jakCastoJist); //nez chce znova jist
					counter += jakCastoJist;
				}

				stav = S_dospele;
				break;
			}
			case S_dospele:{
				int pareniCounter = 0;
				int maxPareni = 20;
				while (true){
					//nejakej cas pred parenim
					int counter = 0;
					while (counter < 1*DEN){
						NajistSe();
						//NechatSeNakrmit();
						Wait(jakCastoJist); //nez chce znova jist
						counter += jakCastoJist;
					}
					if (Random() <= sanceNaSpareni) //sparila se
						break;
					pareniCounter += 1;
					if (maxPareni == pareniCounter){
						if (vypisky)
							cout<<"kralovna se nesparila\n";
                        (new Zabij(this))->Activate();
                        Passivate();
						//Cancel(); //vzdala to a umrela
					}
				}
				stav = S_oplodnene;

				break;
			}
			case S_oplodnene:{                     
				double interval = produkceVajec()*100;
				double posledni = Time;
				int aktMesic = jakyMesic((long)Time);
				while (true){
					if (Time - posledni > DEN){
						NajistSe();//pro zjednoduseni se krmi sama
						interval = produkceVajec()*100;
						if (interval == 0)
							RozeniKralovny(0);
						else
							RozeniKralovny(DEN/interval);
						posledni = Time;
						aktMesic = jakyMesic((long)Time);
					}
					if (interval == 0)
						Wait(DEN);
					else{
						Wait(interval); //kladeni
						if (KAPACITAPLASTVI-med-pocetZarodku > 1){ //POKUD JE PROSTOR V ULU
							//rozeni
							if (vypisky)
								cout << "kralovna rodi\n";

							if (aktMesic == Duben || aktMesic == Kveten || aktMesic == Cerven || aktMesic == Cervenec){
								if (Random() < 0.01){
									(new Trubec)->Activate();
								}
								else
									(new Delnice)->Activate();
							}
							else{
								(new Delnice)->Activate();
							}
						}
						else if (vypisky){
							cout<<"kralovna nema kam rodit, neni misto v plastvych\n";
						}
					}
				}
				break;
			}

			default:{
				if (vypisky)
					cout<<"SPATNY STAV KRALOVNA\n";
				break;
			}
		}

	}

}
void Kralovna::Cancel(){
	zijiciKralovna = NULL;
	if (smrt != NULL)
		smrt->Cancel(); //aby se ji to nesnazilo zabijet po smrti


	//musi dat vedet larvam co se zacnou menit na kralovnu
	for (unsigned int i = 0; i < frontaVickovani.size(); i++){
		Delnice *delnice = (Delnice *)frontaVickovani.GetFirst();
		delnice->prerusen = true;
		delnice->Activate();
	}
	Vcela::Cancel();
}


Delnice::Delnice():lety(0),maxlety(Uniform(15,30)){
	stav = S_vejce;

}

Delnice::Delnice(Stav zacStav, int zacLety):lety(zacLety),maxlety(Uniform(15,30)){
	stav = zacStav;
}

void Delnice::Behavior() {
	Vcela::Behavior();
	while (true){
		switch (stav){
			case S_vejce:{
				Wait(Uniform(70*HODINA,76*HODINA)); //lihnuti vejce
				stav = S_larva;
				break;
			}
			case S_larva:{

				frontaVickovani.Insert(this); //fronta pro aktivovani v pripade smrti kralovny

				//zakukleni
				int counter = 0;
				while (counter < Uniform(4.5*DEN,5.5*DEN)){
					NechatSeNakrmit();
					Wait(jakCastoJist); //nez chce znova jist
					if (prerusen)
						break; //jde zkusit byt kralovnou;
					counter += jakCastoJist;
				}

				if (zijiciKralovna == NULL){
					//zavickovani
					counter = 0;
					while (counter < Uniform(4.5*DEN,5.5*DEN)){
						NechatSeNakrmit();
						Wait(jakCastoJist); //nez chce znova jist
						counter += jakCastoJist;
					}

					Wait(Uniform(6*DEN,8*DEN)); //lihnuti

					if(zijiciKralovna != NULL){ //kralovna ho zabije az se narodi
                        (new Zabij(this))->Activate();
                        Passivate();                    
						//Cancel();
					}
					zijiciKralovna = new Kralovna;
					zijiciKralovna->Activate();
             
                    (new Zabij(this))->Activate();
                    Passivate();
					//Cancel();
                    
				}//neni treba else, uvnitr tohodle skonci
                                                        
				Out();//uz nechce byt ve fronte

				stav = S_kukla;
				break;
			}
			case S_kukla:{
				Wait(Uniform(10*DEN,12*DEN)); //lihnuti delnice
				stav = S_vylihle;

				//ZMENA STAVU
				pocetZarodku -= 1;
				NarozenaVcela();
				break;
			}
			case S_vylihle:{
				pocetKrmicek += 1;

				//pomaha v ulu
				int counter = 0;
				double delka = Normal(20*DEN,5*DEN);
				if (vypisky)
					cout << "bude krmit "<<Name()<<" dnu: "<<delka/DEN<<"\n";
				while (counter < delka){
					NajistSe();
					Wait(jakCastoJist); //nez chce znova jist
					counter += jakCastoJist;
				}
				//pocka az se uvolni
				WaitUntil(pocetKrmicek>0);
				if (vypisky)
					cout<<"uz nebude dal krmit "<<Name()<<" krmila dnu: "<<delka/DEN<<"\n";
				pocetKrmicek -= 1;

				stav = S_dospele;
				break;
			}
			case S_dospele:{
				if (vypisky)
					cout<<"bude litat "<<Name()<<" letu: "<<maxlety<<"\n";
				while (true){
					if (lety >= maxlety){
						if (vypisky)
							cout<<"doletala "<<Name()<<" pocetLetu "<<lety<<"\n";
                        (new Zabij(this))->Activate();
                        Passivate();
						//Cancel(); //umre
					}
					int aktMesic = jakyMesic((long)Time);
					if (aktMesic == Rijen || aktMesic == Listopad || aktMesic == Prosinec|| aktMesic == Leden || aktMesic == Unor){
						NajistSe();
						Wait(jakCastoJist);
						continue;
					}

					Wait(Exponential(0.5*DEN)); //lita
					//NEMUSI NA NIC CEKAT, MEDAR TO KDYZTAK VYBERE
					pridatMed(produkceJidla());
					lety += 1;
				}
				break;
			}
			default:
				if (vypisky)
					cout << "SPATNY STAV DELNICE\n";
				break;
		}
	}


}

void Delnice::Cancel(){
	Vcela::Cancel();
}



int main(int argc, char** argv) // popis experimentu
{
	SetOutput("vcely.dat");
	RandomSeed(time(NULL));


	Init(0, DELKABEHU);

	new HlasicDne;

	(new OdstraneniVaroazy)->Activate(Uniform(260*DEN ,304*DEN));


	for (int i = 0; i < PRACOVKYNE; i++)
		(new Delnice(S_vylihle,0))->Activate();
	for (int i = 0; i < LETAVKY; i++)
		(new Delnice(S_dospele,0))->Activate();

	(new Kralovna(S_oplodnene,3*ROK))->Activate();

	Run(); // beh simulace

	cout<<"\n\n\n\n\n\n";
	cout<<(long)Time/60/60/24<<" dni - konec\n";
	cout<<"Pocatecni med: "<<bunkyNaKilaMedu(pocatecniMed)<<" jinycislo: "<<pocatecniMed<<"\n";
	cout<<"Aktualne medu: "<<bunkyNaKilaMedu(med)<<"  jinycislo: "<<med<<"\n";
	cout<<"Zisk medu: "<<bunkyNaKilaMedu(med)-bunkyNaKilaMedu(pocatecniMed)+bunkyNaKilaMedu(nabranyMed)<<"\n";
	cout<<"Med vybran: "<<pocetVybraniMedu<<"\n";
	cout<<"Pocet rojeni: "<<pocetRojeni<<"\n";
	cout<<"Celkovy pocet umrenych zarodku: "<<pocetZarodkuHlad + pocetZarodkuKrmicky<<"\n";
	cout<<"z toho:\n";
	cout<<"\thlad: "<<pocetZarodkuHlad<<"\n";
	cout<<"\tkrmicky: "<<pocetZarodkuKrmicky<<"\n";
	cout<<"Celkovy pocet umrenych dospelych vcel: "<<pocetVcelyHlad + pocetVcelyKrmicky<<"\n";
	cout<<"z toho:\n";
	cout<<"\thlad: "<<pocetVcelyHlad<<"\n";
	cout<<"\tkrmicky: "<<pocetVcelyKrmicky<<"\n";


	cout<<"Celkem zabitych varoazou: "<<zabitychVaroazou<<"\n";
	cout<<"Celkove vcely total pres celou dobu: "<<celkoveVcely<<"\n";

	for (auto i : DatumVaroazy)
		cout<<"datum varoazy: "<<i/DEN<<"\n";
  	cout<<"\n\n\n\n\n\n";
	DelkaZivota.Output();
	RozeniKralovny.Output();
	ObratMedu.Output();

	ofstream myfile;
	myfile.open("vcely2.dat");
	myfile << "Denni stav medu\n";
	for (int i = 0 ; i < DELKABEHU/DEN; i++){
		myfile <<i+1<<"\t"<< DenniStavMedu[i]<<"\n";
	}
	myfile << "Denni stav vcel\n";
	for (int i = 0 ; i < DELKABEHU/DEN; i++){
		myfile <<i+1<<"\t"<< DenniStavVcel[i]<<"\n";
	}
	myfile << "Denni stav zarodku\n";
	for (int i = 0 ; i < DELKABEHU/DEN; i++){
		myfile <<i+1<<"\t"<< DenniStavZarodku[i]<<"\n";
	}
	myfile << "Denni stav trubcu\n";
	for (int i = 0 ; i < DELKABEHU/DEN; i++){
		myfile <<i+1<<"\t"<< DenniStavTrubcu[i]<<"\n";
	}
	myfile.close();
}
