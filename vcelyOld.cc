#include "simlib.h"
#include "vcely.h"

#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

#define DELNICE 10

#define DEN 60*60*24
#define ROK 365*DEN
#define HODINA 60*60
#define MINUTA 60

#define MAXKAPJIDLO 5000


Store KapacitaJidla("kapacita jidla v ulu",MAXKAPJIDLO);
Store Krmicky("sklad krmicek",0);
int stavJidla = 0;
int pocetKrmicek = 0;


bool varoaza = false;
bool kralovnaZije = false;

Queue frontaVickovani;
Queue fronta;

#define MAXVCEL 50
int pocetVcel = 0;
vector<Delnice *> zijiciDelnice;
vector<Trubec *> zijiciTrubci;
Kralovna * zijiciKralovna = NULL;

Histogram celk("Celkova doba v systemu", 0, 5, 24);



int eggsMonth(){
	return 5*HODINA;
}



void removeFromVector(vector<Vcela *> *vec, Vcela * proc){
	int index = 0;
	for (auto i : *vec){
		if (i == proc){
			vec->erase((vec->begin() + index));
		}
		index += 1;
	}
}
void oddelatZtrubcu(Trubec * proc){
	int index = 0;
	for (auto i : zijiciTrubci){
		if (i == proc){
			zijiciTrubci.erase((zijiciTrubci.begin() + index));
		}
		index += 1;
	}
}
void oddelatZdelnic(Delnice * proc){
	int index = 0;
	for (auto i : zijiciDelnice){
		if (i == proc){
			zijiciDelnice.erase((zijiciDelnice.begin() + index));
		}
		index += 1;
	}
}

class Rojeni : public Event {
	void Behavior(){
		cout<<"rojeni\n";

		if (zijiciKralovna != NULL){
			zijiciKralovna->Cancel();
		}


		int pulka = zijiciDelnice.size()/2;
		for (int i = 0; i < pulka; i++){
			int index = Uniform(0,zijiciDelnice.size());
			zijiciDelnice[index]->Cancel();
		}

		pulka = zijiciTrubci.size()/2;
		for (int i = 0; i < pulka; i++){
			int index = Uniform(0,zijiciTrubci.size());
			zijiciTrubci[index]->Cancel();
		}
	}
};

class Smrt : public Event {
	Vcela *vcelicka;
public:
	Smrt(Vcela *p, double dt):vcelicka(p){
		Activate(Time+dt);
	}
	void Behavior(){
		vcelicka->Cancel();
		Cancel();
	}
};

class Jezeni : public Event { //jezeni v intervalu
	Vcela *vcelicka;
public:
	Jezeni(Vcela *p):vcelicka(p){
	}
	void Behavior(){
		if (stavJidla == 0){
			vcelicka->Cancel(); //umre hladem
			Cancel();
		}
		else{
			stavJidla -= 1;
			vcelicka->Leave(KapacitaJidla,1);
			Activate(Time+0.5*DEN);
		}
	}
};

class Najistse : public Event { //naji se jednou, bez opakovani
	Vcela *vcelicka;
public:
	Najistse(Vcela *p):vcelicka(p){
	}
	void Behavior(){
		if (stavJidla == 0){
			vcelicka->Cancel(); //umre hladem
			Cancel();
		}
		else{
			stavJidla -= 1;
			vcelicka->Leave(KapacitaJidla,1);
			Cancel();
		}
	}
};


class Dokrmeno : public Event {
	Vcela *vcelicka;
public:
	Dokrmeno(Vcela *p):vcelicka(p){
	}
	void Behavior(){
		vcelicka->Leave(Krmicky,1);
		Cancel();
	}
};



Krmeni::Krmeni(Vcela *p):vcelicka(p){
}
void Krmeni::Behavior(){
	while (true){
		if (pocetKrmicek == 0){
			vcelicka->krmeni = NULL;
			cout<<this<<" krmeni "<<vcelicka->krmeni<<"\n";
			vcelicka->Cancel(); //umre hladem
			Cancel();
		}
		else if(stavJidla == 0){
			vcelicka->krmeni = NULL;
			vcelicka->Cancel(); //umre hladem
			Cancel();
		}
		else{
			cout << pocetKrmicek<<" pocet krmicek\n";
			pocetKrmicek -= 1;

			//Enter(Krmicky);
			stavJidla -= 1;
			cout<<stavJidla<<" stav jidla \n";

			//(new Dokrmeno(vcelicka))->Activate(Time+MINUTA);
			Wait(Time+MINUTA);
			//Leave(Krmicky);

			pocetKrmicek += 1;
			cout << pocetKrmicek<<" pocet krmicek\n";

			//Activate(Time+0.5*DEN);
			Wait(Time+0.5*DEN);
		}
	}
}

class Timeout : public Event {
public:
	Vcela *vcela;
	Timeout(Vcela *p) : vcela(p)
	{
	}
	void Behavior(){
		cout<<"timeout "<<Time<< "\n";
		vcela->Activate();
	}
};

class Vrazdy : public Process{
public:
	Vcela *vcela;
	Vrazdy(Vcela *moznaKralovna):vcela(moznaKralovna)
	{
	}
	void Behavior() {
		fronta.Insert(this);
		Passivate();
		vcela->Cancel();
		Cancel();
	}
};

void Vcela::NechatSeNakrmit(){
	if (pocetKrmicek == 0){
		Cancel();
	}
	else if(stavJidla == 0){
		Cancel();
	}
	else{
		cout << pocetKrmicek<<" pocet krmicek\n";
		pocetKrmicek -= 1;
		stavJidla -= 1;
		cout<<stavJidla<<" stav jidla \n";
		Wait(Time+MINUTA);
		pocetKrmicek += 1;
		cout << pocetKrmicek<<" pocet krmicek\n";
	}
}


void Vcela::Behavior(){
	narozeni = Time;
	cout<<"zije "<<Name()<<"\n";
	pocetVcel += 1;
	if (pocetVcel >= MAXVCEL){
		(new Rojeni)->Activate();
	}
}

void Vcela::Cancel(){
	cout<<"umira "<<Name()<<"\n";
	celk(Time-narozeni);
	pocetVcel -= 1;
	Process::Cancel(); //PROC PADA?
}


void Trubec::Behavior() {
	Vcela::Behavior();
	zijiciTrubci.push_back(this);
	smrt = new Smrt(this,20*DEN);

	Wait(3*DEN); //lihnuti vejce
	krmeni = new Krmeni(this);
	krmeni->Activate(); //hned se chce najist
	Wait(7*DEN); //zakukleni
	krmeni->Cancel(); //uz se nenechava krmit
	krmeni = NULL;

	Wait(13*DEN); //lihnuti trubce
	Jezeni *jezeni = new Jezeni(this);
	jezeni->Activate();
	Wait(8*DEN); //dospivani
	jezeni->Cancel(); //uz neji porad

	while (true){
		(new Najistse(this))->Activate(); //naji se na cestu
		Wait(0.5*DEN); //leti a hleda

		if(Random() <= 0.5){ //nesparil
			if(Random() <= 0.1) //donesl varoazu
				varoaza = true;
		}
		else{ //sparil, umira
			Cancel();
		}
	}
}

void Trubec::Cancel() {
	smrt->Cancel(); //aby se ji to nesnazilo zabijet po smrti

	if (krmeni != NULL){//pokud ji nezabilo krmeni
		krmeni->Cancel();
		krmeni = NULL;
	}
	oddelatZtrubcu(this);
	Vcela::Cancel();
}





Kralovna::Kralovna():stav(S_vylihle),vek(0){
	SetName("kralovna");
	zijiciKralovna = this;
}

Kralovna::Kralovna(Stav zacStav, int zacVek):stav(zacStav),vek(zacVek){
	SetName("kralovna");
	zijiciKralovna = this;
}
void Kralovna::Behavior(){
	Vcela::Behavior();
	kralovnaZije = true;
	smrt = new Smrt(this,5*ROK-vek);
	//matku krmi cely zivot
	krmeni = new Krmeni(this);
	krmeni->Activate(Time+10);


	while (true){
		switch (stav){
			case S_vylihle:{
				Wait(3*DEN); //dorusta
				stav = S_dospele;
				break;
			}
			case S_dospele:{
				while (true){ //kdyz se parila mockrat - TODO
					Wait(2*DEN); //nejakej cas pred parenim
					if (Random() <= 0.75) //sparila se
						break;
				}
				stav = S_oplodnene;
				break;
			}
			case S_oplodnene:{
				double interval = eggsMonth();
				double posledni = Time;
				while (true){
					if (Time - posledni > DEN){
						interval = eggsMonth();
						posledni = Time;
					}
					Wait(interval); //kladeni
					//rozeni
					(new Delnice)->Activate();
				}
				break;
			}

			default:{
				break;
			}
		}

	}


}
void Kralovna::Cancel(){
	zijiciKralovna = NULL;
	smrt->Cancel(); //aby se ji to nesnazilo zabijet po smrti

	cout<<"kralovna "<<krmeni<<"\n";

	if (krmeni != NULL){//pokud ji nezabilo krmeni
		krmeni->Cancel();
		krmeni = NULL;
	}
	kralovnaZije = false;
	//musi dat vedet larvam co se zacnou menit na kralovnu
	for (unsigned int i = 0; i < frontaVickovani.size(); i++){
		Delnice *delnice = (Delnice *)frontaVickovani.GetFirst();
		delnice->prerusen = true;
		delnice->Activate();
	}
	Vcela::Cancel();
}


Delnice::Delnice():stav(S_vejce),lety(0){
}

Delnice::Delnice(Stav zacStav, int zacLety):stav(zacStav),lety(zacLety){
}

void Delnice::Behavior() {
	Vcela::Behavior();
	zijiciDelnice.push_back(this);
	while (true){
		switch (stav){
			case S_vejce:{
				Wait(3*DEN); //lihnuti vejce
				stav = S_larva;
				break;
			}
			case S_larva:{
				frontaVickovani.Insert(this); //fronta pro aktivovani v pripade smrti kralovny
				krmeni = new Krmeni(this);
				krmeni->Activate(); //hned se chce najist
				Wait(5*DEN); //zakukleni
				if (krmeni != NULL){
					krmeni->Cancel();
					krmeni = NULL;
				}
				if (!prerusen)//pokud neumrela kralovna, musi se vyndat z fronty
					Out();


				if (!kralovnaZije){
					Vrazdy *vrazdy = new Vrazdy(this);
					vrazdy->Activate();
					Wait(4*DEN); //zavickovani
					Wait(7*DEN); //lihnuti

					if(kralovnaZije){
						vrazdy->Cancel();
						Cancel();
					}
					kralovnaZije = true;
					vrazdy->Cancel();
					//zabije ostatni
					while (!fronta.Empty()){
						fronta.GetFirst()->Activate();
					}
					(new Kralovna)->Activate();
					Cancel();
				}//neni treba else, uvnitr tohodle skonci

				Wait(5*DEN); //zavickovani
				stav = S_kukla;
				break;
			}
			case S_kukla:{
				Wait(11*DEN); //lihnuti delnice
				stav = S_vylihle;
				break;
			}
			case S_vylihle:{//TODO DELNICE MUSI JIST
				//Krmicky.SetCapacity(Krmicky.Capacity()+1);

				pocetKrmicek += 1;

				Wait(30*DEN); //pomaha v ulu
				//TODO NECEKAJI AZ DOKRMI, ALE RUSI KAPACITU ROVNOU

				WaitUntil(pocetKrmicek>0);
				pocetKrmicek -= 1;

				//Krmicky.SetCapacity(5);
				stav = S_dospele;
				break;
			}
			case S_dospele:{//TODO DELNICE MUSI JIST
				while (true){
					Enter(KapacitaJidla,1);
					Wait(0.5*DEN); //lita
					stavJidla += 1;
					lety += 1;
					if (lety == 30){
						Cancel(); //umre
					}
				}
				break;
			}
			default:
				break;
		}
	}


}

void Delnice::Cancel(){
	cout<<krmeni<<" cancel delnice "<<this<<"\n";
	if (krmeni != NULL){//pokud ji nezabilo krmeni
		krmeni->Cancel();
		krmeni = NULL;
	}
	oddelatZdelnic(this);
	Vcela::Cancel();
}

class RusickaKapacity :public Process{
public:
	int kolik;
	RusickaKapacity(int kap):kolik(kap){

	}
	void Behavior(){
		Enter(KapacitaJidla,kolik);
	}
};

int main(int argc, char** argv) // popis experimentu
{
	SetOutput("vcely.dat");
	RandomSeed(time(NULL));


	Init(0, 1000*DEN);

	stavJidla = 500;
	(new RusickaKapacity(stavJidla))->Activate();

	(new Kralovna)->Activate();

	string names[DELNICE]; //wtf nepretrvavajici stringy co to ma byt za vtip
	for (int i = 0; i < DELNICE; i++){
		Delnice *delnice;
		//(new Delnice)->Activate();
		delnice = new Delnice(S_vylihle,0);
		string name = "Delnice-"+to_string(i);
		names[i] = name;

		delnice->SetName(names[i].c_str());
		delnice->Activate();
	}



	Run(); // beh simulace


	celk.Output();
}
