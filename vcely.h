#include "simlib.h"

#define DEN (60*60*24)
#define TYDEN (7*DEN)
#define ROK (365*DEN)
#define HODINA (60*60)
#define MINUTA 60

enum Stav {S_vejce,S_larva,S_kukla,S_vylihle,S_dospele,S_oplodnene};
enum Mesic {Leden,Unor,Brezen,Duben,Kveten,Cerven,Cervenec,Srpen,Zari,Rijen,Listopad,Prosinec};

class Smrt;


class Varoaza : public Event{
public:
	double narozeni;
	double interval;
	Varoaza();
	void Behavior();
};

class Vcela : public Process{
public:
	Stav stav;
	bool inicializovana = false; //pokud dostala jiny vychozi nez pocatecni stav
	double narozeni;
	bool mamKrmicku = false;
	Smrt *smrt = NULL;

	void Behavior();
	virtual void Cancel();
	void NechatSeNakrmit();
	void NajistSe();
	void NarozenaVcela(); //ne vajicko larva ani kukla

};



class Trubec : public Vcela {
private:
	int vek;
public:
	void Behavior();
	void Cancel();
	Trubec(); //
	Trubec(Stav zacStav, int zacVek);
};

class Kralovna : public Vcela {
private:
	int vek;

public:
	Kralovna();
	Kralovna(Stav zacStav, int zacVek);
	void Behavior();
	void Cancel();
};

class Delnice : public Vcela {
private:
	int lety;
	int maxlety;

public:
	Delnice();
	Delnice(Stav zacStav, int zacLety);
	bool prerusen = false;
	void Behavior();
	void Cancel();
	void SmrtHladem();
};


