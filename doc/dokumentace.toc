\select@language {czech}
\contentsline {chapter}{\numberline {1}\IeC {\'U}vod}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Auto\IeC {\v r}i projektu a spolupr\IeC {\'a}ce}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Validita modelu}{1}{section.1.2}
\contentsline {chapter}{\numberline {2}Rozbor t\IeC {\'e}matu a pou\IeC {\v z}it\IeC {\'y}ch metod/technologi\IeC {\'\i }}{2}{chapter.2}
\contentsline {paragraph}{V\IeC {\v c}el\IeC {\'\i } \IeC {\'u}l}{2}{section*.3}
\contentsline {paragraph}{V\IeC {\v c}elstvo}{2}{section*.4}
\contentsline {paragraph}{Matka}{2}{section*.5}
\contentsline {paragraph}{D\IeC {\v e}lnice}{2}{section*.7}
\contentsline {paragraph}{Trubec}{3}{section*.8}
\contentsline {paragraph}{Varroa destructor}{3}{section*.9}
\contentsline {paragraph}{Rojen\IeC {\'\i }}{3}{section*.10}
\contentsline {paragraph}{V\IeC {\v c}ela\IeC {\v r}}{3}{section*.11}
\contentsline {section}{\numberline {2.1}Popis pou\IeC {\v z}it\IeC {\'y}ch postup\IeC {\r u} pro vytvo\IeC {\v r}en\IeC {\'\i } modelu}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Popis p\IeC {\r u}vodu pou\IeC {\v z}it\IeC {\'y}ch metod/technologi\IeC {\'\i }}{4}{section.2.2}
\contentsline {chapter}{\numberline {3}Koncepce}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Zp\IeC {\r u}sob vyj\IeC {\'a}d\IeC {\v r}en\IeC {\'\i } konceptu\IeC {\'a}ln\IeC {\'\i }ho modelu}{6}{section.3.1}
\contentsline {section}{\numberline {3.2}Formy konceptu\IeC {\'a}ln\IeC {\'\i }ho modelu}{6}{section.3.2}
\contentsline {chapter}{\numberline {4}Architektura simula\IeC {\v c}n\IeC {\'\i }ho modelu/simul\IeC {\'a}toru}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}Mapov\IeC {\'a}n\IeC {\'\i } abstraktn\IeC {\'\i }ho modelu do simula\IeC {\v c}n\IeC {\'\i }ho}{7}{section.4.1}
\contentsline {chapter}{\numberline {5}Podstata simula\IeC {\v c}n\IeC {\'\i }ch experiment\IeC {\r u} a jejich pr\IeC {\r u}b\IeC {\v e}h}{8}{chapter.5}
\contentsline {section}{\numberline {5.1}Postup experimentov\IeC {\'a}n\IeC {\'\i }}{8}{section.5.1}
\contentsline {section}{\numberline {5.2}Dokumentace jednotliv\IeC {\'y}ch experiment\IeC {\r u}}{8}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Norm\IeC {\'a}ln\IeC {\'\i } stav}{8}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Matce se nepoda\IeC {\v r}ilo sp\IeC {\'a}\IeC {\v r}it}{9}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Zru\IeC {\v s}en\IeC {\'\i } tlumen\IeC {\'\i } varo\IeC {\'a}zy}{9}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Studen\IeC {\'e} l\IeC {\'e}to}{10}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Rojen\IeC {\'\i }}{10}{subsection.5.2.5}
\contentsline {section}{\numberline {5.3}Z\IeC {\'a}v\IeC {\v e}ry experiment\IeC {\r u}}{10}{section.5.3}
\contentsline {chapter}{\numberline {6}Shrnut\IeC {\'\i } simula\IeC {\v c}n\IeC {\'\i }ch experiment\IeC {\r u} a z\IeC {\'a}v\IeC {\v e}r}{11}{chapter.6}
